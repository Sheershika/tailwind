import React, { useState, useMemo, ChangeEvent } from "react";

function App() {
  const [task, setTask] = useState<Array<Todo>>([
    { id: 1, title: "Complete Task-1", status: false },
  ]);

  const [newTask, setNewTask] = useState<string>("");

  const addTask = () => {
    let num = task.length + 1;
    let newData = { id: num, title: newTask, status: false };
    setTask([...task, newData]);
    setNewTask("");
  };

  const deleteTask: DeleteTask = (id: number) => {
    let newData: Todo[] = task.filter((data) => data.id !== id);
    setTask(newData);
  };

  const toggleComplete: ToggleComplete = (id: number) => {
    const updatedTodos: Todo[] = task.map((data) => {
      if (data.id === id) {
        return { ...data, status: !data.status };
      }
      return data;
    });
    setTask(updatedTodos);
  };

  const setsNewTask = (e: ChangeEvent<HTMLInputElement>) => {
    setNewTask(e.target.value);
  };

  const [edit, setEdit] = useState<Todo>();

  const editTask: EditTask = (id: number) => {
    const find: Todo | undefined = task.find((item) => item.id === id);
    setEdit(find);
  };

  const onEditChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (edit) {
      const newEditValue: Todo = {
        ...edit,
        title: e.target.value,
      };
      setEdit(newEditValue);
    }
  };

  const onSave = () => {
    const newTask = [...task];
    const findTaskIndex: number = task.findIndex(
      (item) => item.id === edit?.id
    );
    if (findTaskIndex >= 0 && edit) {
      newTask[findTaskIndex] = edit;
      setTask(newTask);
      setEdit(undefined);
    }
  };

  const [todoFilter, setTodoFilter] = useState<string | undefined>(undefined);

  const filterData = task.filter((data) => {
    if (todoFilter === "false") {
      return data.status === true;
    } else if (todoFilter === "true") {
      return data.status === false;
    } else {
      return data;
    }
  });

  const isFilter = todoFilter !== undefined;

  const printTask: Todo[] = isFilter ? filterData : task;

  let activeCount = useMemo(() => {
    let count = 0;

    if (printTask) {
      printTask.forEach((task) => {
        if (!task.status) {
          count++;
        }
      });
    }
    return count;
  }, [task, todoFilter]);

  return (
    <div>
      <div className=" text-center text-5xl font-extrabold mt-24">
        TAILWIND- TODO LIST
      </div>
      <div className="bg-blue-300 text-2xl text-center p-20 w-max leading-10 font-serif m-auto mt-40 border-black border-4 rounded-3xl hover:bg-blue-200">
        <div className="text-3xl font-semibold mb-8 ">
          What needs to be done?
        </div>
        <div>
          <input
            className="bg-blue-50  border-black border-4 w-max rounded-full mb-3  hover:bg-blue-400"
            type="text"
            value={newTask}
            onChange={setsNewTask}
          ></input>
          <span>
            <button
              onClick={addTask}
              className="border-black border-2 text-base rounded-xl p-2 font-semibold ml-2 bg-orange-100 hover:bg-red-400"
            >
              Add Task
            </button>
          </span>
        </div>
        <div>
          <select
            className="bg-blue-50 p-2 text-base border-black border-4 w-max rounded-full"
            placeholder="Select Option"
            value={todoFilter}
            onChange={(e) => {
              setTodoFilter(e.target.value);
            }}
          >
            <option value="all">All</option>
            <option value="true">Active Tasks</option>
            <option value="false">Completed Tasks</option>
          </select>
        </div>

        <h1>{activeCount} tasks remaining</h1>

        <div>
          {printTask && printTask.length ? "" : "No Tasks..."}

          {printTask &&
            printTask.map((data) => {
              return (
                <div>
                  <div>
                    <div>
                      <div className="bg-blue-50 flex flex-row justify-items-center p-4 rounded-3xl border-4 mb-2 border-black hover:bg-blue-400">
                      <input
                          className="w-6 mr-2 border-black border-10 rounded-full"
                          type="checkbox"
                          onChange={() => toggleComplete(data.id)}
                        />
                        <div className = "border-4 mr-2 border-black rounded-full  bg-orange-100 p-1">{data.id}</div>
                        <div className={data.status ? " line-through" : ""}>
                          {data.id === edit?.id ? (
                            <input
                              className=" border-black place-content-center border-4 w-max rounded-full"
                              value={edit.title}
                              onChange={onEditChange}
                            />
                          ) : (
                            data.title
                          )}
                        </div>
                        <div className=" line-through">{data.status}</div>
                        
                        {data.status ? null : (
                          <button  className="border-black border-2 text-base rounded-xl p-1 font-semibold ml-2 bg-orange-100  hover:bg-red-400" onClick={() => editTask(data.id)}>
                            Edit
                          </button>
                        )}
                        {data.id === edit?.id && (
                          <button  className="border-black border-2 text-base rounded-xl p-1 font-semibold ml-2 bg-orange-100  hover:bg-red-400" onClick={onSave}>Save</button>
                        )}
                        <button  className="border-black border-2 text-base rounded-xl p-1 font-semibold ml-2 bg-orange-100  hover:bg-red-400" onClick={() => deleteTask(data.id)}>
                          Delete
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
        </div>
      </div>
    </div>
  );
}

export default App;
