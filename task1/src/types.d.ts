type Todo = {
    id: number;
    title: string;
    status: boolean;
}

type AddTask = (newTask: Todo) => void;

type DeleteTask = (id: number) => void;

type ToggleComplete = (id: number) => void;

type EditTask = (id: number) => void;